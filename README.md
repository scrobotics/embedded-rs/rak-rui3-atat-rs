# AT Driver for the RAK3172 WisDuo LPWAN Module

This respository contains a driver for the [RAK3172](https://docs.rakwireless.com/Product-Categories/WisDuo/RAK3172-Module/Overview/) module running the RUI3 (RAK Unified Interface V3) firmware in [AT mode](https://docs.rakwireless.com/RUI3/Serial-Operating-Modes/AT-Command-Manual/). The driver is built on top of the [atat crate](https://docs.rs/atat/latest/atat/).

The crate is available on [crates.io](https://crates.io/crates/rak-rui3-eh-driver).

## TODO
- [ ] Create a serializer similar to serde_at to support the limitations of the AT mode that we mentioned previously.
- [ ] Remove STD dependencies

## Developer notes about RUI3 AT protocol

The module doesn't really comply with the AT standard. In fact, the behavior of the module doesn't even match the official documentation. The following are the main differences we've found:

* There's no `\r\n` marking the beginning of a response
* It uses test commands (`AT+FOO=?`) instead of read commands (`AT+FOO?`)
* Command parameters are separated by semicolons (`:`) instead of commas (`,`)

To overcome the differences we had to build [our own digester](src/digester.rs) (i.e. the command parser in the atat world).

## Examples

* `linux-simple`: example of interacting with the LoRaWAN module connected through a serial converter to a Linux computer. Requires the converter to be `/dev/ttyUSB0`.
    ```
    cd examples/linux_simple
    cargo run
    ```

For a more complete example visit Tested with the [Dark Sky Meter Firmware](https://gitlab.com/scrobotics/optical-makerspace/dark-sky-meter-fw).
