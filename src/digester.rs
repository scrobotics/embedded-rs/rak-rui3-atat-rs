use atat::digest::ParseError;
use atat::{nom, DigestResult, Digester, Parser};

use nom::branch::alt;
use nom::bytes::complete::{is_not, tag, take_until};
use nom::bytes::streaming::tag_no_case;
use nom::combinator::{opt, recognize, success};
use nom::sequence::tuple;

use crate::rak_rui3_atat::urc::UrcMessages;

#[derive(Default)]
pub struct RakRui3Digester {}

impl RakRui3Digester {
    pub fn echo(buf: &[u8]) -> nom::IResult<&[u8], &[u8]> {
        if buf.len() < 2 {
            return Ok((buf, &[]));
        }

        recognize(Self::take_until_including("\n\r"))(buf)
    }

    pub fn take_until_including<T, Input, Error: nom::error::ParseError<Input>>(
        tag: T,
    ) -> impl Fn(Input) -> nom::IResult<Input, (Input, Input), Error>
    where
        Input: nom::Compare<T> + nom::FindSubstring<T> + nom::InputLength + nom::InputTake,
        T: nom::InputLength + Clone + nom::InputTake,
    {
        move |i| {
            let (i, d) = take_until(tag.clone())(i)?;
            let (i, t) = tag_no_case(tag.clone())(i)?;
            Ok((i, (d, t)))
        }
    }

    pub fn success_response(buf: &[u8]) -> nom::IResult<&[u8], (DigestResult, usize)> {
        let (i, ((data, tg), ws)) =
            tuple((Self::take_until_including("OK\r\n"), success(&b""[..])))(buf)?;

        match data.len() {
            0 => {
                return Ok((
                    i,
                    (
                        DigestResult::Response(Ok(Self::trim_ascii_whitespace(data))),
                        data.len() + tg.len() + ws.len(),
                    ),
                ))
            }
            _ => {
                let (payload, (_, _, _)) = tuple((is_not("="), tag("="), success(&b""[..])))(data)?;

                return Ok((
                    i,
                    (
                        DigestResult::Response(Ok(Self::trim_ascii_whitespace(payload))),
                        data.len() + tg.len() + ws.len(),
                    ),
                ));
            }
        }
    }

    pub fn error_response(buf: &[u8]) -> nom::IResult<&[u8], (DigestResult, usize)> {
        let (i, ((data, tag), ws)) = alt((
            tuple((
                Self::take_until_including("AT_ERROR\r\n"),
                success(&b""[..]),
            )),
            tuple((
                Self::take_until_including("AT_PARAM_ERROR\r\n"),
                success(&b""[..]),
            )),
            tuple((
                Self::take_until_including("AT_BUSY_ERROR\r\n"),
                success(&b""[..]),
            )),
            tuple((
                Self::take_until_including("AT_TEST_PARAM_OVERFLOW\r\n"),
                success(&b""[..]),
            )),
            tuple((
                Self::take_until_including("AT_NO_CLASSB_ENABLE\r\n"),
                success(&b""[..]),
            )),
            tuple((
                Self::take_until_including("AT_NO_NETWORK_JOINED\r\n"),
                success(&b""[..]),
            )),
            tuple((
                Self::take_until_including("AT_RX_ERROR\r\n"),
                success(&b""[..]),
            )),
        ))(buf)?;

        Ok((
            i,
            (
                DigestResult::Response(Err(atat::InternalError::Error)),
                data.len() + tag.len() + ws.len(),
            ),
        ))
    }

    fn trim_ascii_whitespace(x: &[u8]) -> &[u8] {
        let from = match x.iter().position(|x| !x.is_ascii_whitespace()) {
            Some(i) => i,
            None => return &x[0..0],
        };
        let to = x.iter().rposition(|x| !x.is_ascii_whitespace()).unwrap();
        &x[from..=to]
    }

    pub fn trim_start_ascii_space(x: &[u8]) -> &[u8] {
        match x.iter().position(|&x| x != b' ') {
            Some(offset) => &x[offset..],
            None => &x[0..0],
        }
    }

    pub fn find_response_start(x: &[u8]) -> &[u8] {
        match x.iter().position(|&x| x == b'=') {
            Some(offset) => &x[offset + 1..],
            None => &x,
        }
    }
}

impl Digester for RakRui3Digester {
    fn digest<'a>(&mut self, input: &'a [u8]) -> (DigestResult<'a>, usize) {
        // 1. Optionally discard space and echo
        let buf = Self::trim_start_ascii_space(input);
        let space_bytes = input.len() - buf.len();
        let (buf, space_and_echo_bytes) = match opt(Self::echo)(buf) {
            Ok((buf, echo)) => (buf, space_bytes + echo.unwrap_or_default().len()),
            Err(nom::Err::Incomplete(_)) => return (DigestResult::None, 0),
            Err(_) => panic!("NOM ERROR - opt(echo)"),
        };

        // Incomplete. Eat the echo and do nothing else.
        let incomplete = (DigestResult::None, space_and_echo_bytes);

        // 2. Match for URC's
        match <UrcMessages as Parser>::parse(input) {
            Ok((urc, len)) => return (DigestResult::Urc(urc), len),
            Err(ParseError::Incomplete) => return incomplete,
            _ => {}
        }

        // 3. Parse for success responses
        match Self::success_response(buf) {
            Ok((_, (result, len))) => return (result, len),
            Err(nom::Err::Incomplete(_)) => return incomplete,
            _ => {}
        }

        // 4. Parse for error responses
        if let Ok((_, (result, len))) = Self::error_response(input) {
            return (result, len);
        }

        // No matches at all.
        incomplete
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use atat::heapless;

    const TEST_RX_BUF_LEN: usize = 256;

    #[test]
    fn urc_digest() {
        let mut digester = RakRui3Digester::default();
        let mut buf = heapless::Vec::<u8, TEST_RX_BUF_LEN>::new();

        buf.extend_from_slice(b"+EVT:JOINED\r\n").unwrap();
        let (res, bytes) = digester.digest(&buf);
        assert_eq!((res, bytes), (DigestResult::Urc(b"+EVT:JOINED"), 13));

        buf.rotate_left(bytes);
        buf.truncate(buf.len() - bytes);
        assert!(buf.is_empty());
    }

    #[test]
    fn success_digest() {
        let mut digester = RakRui3Digester::default();
        let mut buf = heapless::Vec::<u8, TEST_RX_BUF_LEN>::new();

        buf.extend_from_slice(b"OK\r\n").unwrap();
        let (res, bytes) = digester.digest(&buf);
        assert_eq!((res, bytes), (DigestResult::Response(Ok(&b""[..])), 4));

        buf.rotate_left(bytes);
        buf.truncate(buf.len() - bytes);
        assert!(buf.is_empty());

        buf.extend_from_slice(b"AT+DEVADDR=01020A0B\r\nOK\r\n")
            .unwrap();
        let (res, bytes) = digester.digest(&buf);
        assert_eq!(
            (res, bytes),
            (DigestResult::Response(Ok(&b"01020A0B"[..])), 25)
        );

        buf.rotate_left(bytes);
        buf.truncate(buf.len() - bytes);
        assert!(buf.is_empty());
    }

    #[test]
    fn error_digest() {
        let mut digester = RakRui3Digester::default();
        let mut buf = heapless::Vec::<u8, TEST_RX_BUF_LEN>::new();

        buf.extend_from_slice(b"AT_ERROR\r\n").unwrap();
        let (res, bytes) = digester.digest(&buf);
        assert_eq!(
            (res, bytes),
            (DigestResult::Response(Err(atat::InternalError::Error)), 10)
        );

        buf.rotate_left(bytes);
        buf.truncate(buf.len() - bytes);
        assert!(buf.is_empty());
    }
}
