use thiserror::Error;

pub mod digester;
pub mod modem;
pub mod rak_rui3_atat;

#[derive(Debug, Error)]
pub enum ModemError {
    #[error("Modem not responding")]
    NotResponding,
    #[error("Not attached to network")]
    NoNetwork,
    #[error("Requested feature is not yet supported")]
    NotSupported,
    #[error("Request timeout")]
    OperationTimeout,
    #[error("Request timeout")]
    OperationFailed,
}
