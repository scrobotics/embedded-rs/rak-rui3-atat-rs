use atat::atat_derive::AtatEnum;

#[derive(Debug, Clone, PartialEq, AtatEnum)]
#[repr(u8)]
pub enum JoinRequest {
    Stop = 0,
    Start = 1,
}

#[derive(Debug, Clone, PartialEq, AtatEnum)]
#[repr(u8)]
pub enum ConfirmMode {
    AckOff = 0,
    AckOn = 1,
}

#[derive(Debug, Clone, PartialEq, AtatEnum)]
#[repr(u8)]
pub enum TimeRequestStatus {
    Disabled = 0,
    Enabled = 1,
}

#[cfg(feature = "eu868")]
#[derive(Debug, Clone, PartialEq, AtatEnum)]
#[repr(u8)]
pub enum DataRate {
    SF12BW125 = 0,
    SF11BW125 = 1,
    SF10BW125 = 2,
    SF9BW125 = 3,
    SF8BW125 = 4,
    SF7BW125 = 5,
    SF7BW250 = 6,
    FSK50 = 7,
}
