pub mod responses;
pub mod types;
pub mod urc;

use atat::atat_derive::AtatCmd;
use atat::heapless_bytes::Bytes;

use responses::*;
use types::*;

#[derive(Clone, AtatCmd)]
#[at_cmd("", NoResponse, timeout_ms = 1000)]
pub struct AT;

/// Read the device serial number.
#[derive(Clone, AtatCmd)]
#[at_cmd("+SN=?", SN, timeout_ms = 300)]
pub struct GetSN;

/// Get firmware version.
#[derive(Clone, AtatCmd)]
#[at_cmd("+VER=?", Version, timeout_ms = 300)]
pub struct GetVersion;

/// Read the device end unique ID (OTAA mode).
#[derive(Clone, AtatCmd)]
#[at_cmd("+DEVEUI=?", DevEUI, timeout_ms = 300)]
pub struct GetDevEUI;

/// Set the device end unique ID (OTAA mode).
#[derive(Clone, AtatCmd)]
#[at_cmd("+DEVEUI", NoResponse, timeout_ms = 300)]
pub struct SetDevEUI {
    #[at_arg(position = 1)]
    pub dev_eui: Bytes<16>,
}

/// Read the unique application identifier (OTAA mode).
#[derive(Clone, AtatCmd)]
#[at_cmd("+APPEUI=?", AppEUI, timeout_ms = 300)]
pub struct GetAppEUI;

/// Set the unique application identifier (OTAA mode).
#[derive(Clone, AtatCmd)]
#[at_cmd("+APPEUI", NoResponse, timeout_ms = 300)]
pub struct SetAppEUI {
    #[at_arg(position = 1)]
    pub app_eui: Bytes<16>,
}

/// Read the application key (OTAA mode).
#[derive(Clone, AtatCmd)]
#[at_cmd("+APPKEY=?", AppKey, timeout_ms = 300)]
pub struct GetAppKey;

/// Set the unique application identifier (OTAA mode).
#[derive(Clone, AtatCmd)]
#[at_cmd("+APPKEY", NoResponse, timeout_ms = 300)]
pub struct SetAppKey {
    #[at_arg(position = 1)]
    pub app_key: Bytes<32>,
}

/// Join LoRaWAN Network
#[derive(Clone, AtatCmd)]
#[at_cmd("+JOIN", NoResponse, timeout_ms = 300)]
pub struct JoinNetwork {
    #[at_arg(position = 1)]
    pub request: JoinRequest,
}

/// Set Data Rate (SF & BW)
#[derive(Clone, AtatCmd)]
#[at_cmd("+DR", NoResponse, timeout_ms = 300)]
pub struct SetDataRate {
    #[at_arg(position = 1)]
    pub rate: DataRate,
}

/// Get Network Join Status
#[derive(Clone, AtatCmd)]
#[at_cmd("+NJS=?", JoinStatus, timeout_ms = 300)]
pub struct GetJoinStatus;

/// Set Send Confirm Mode
#[derive(Clone, AtatCmd)]
#[at_cmd("+CFM", NoResponse, timeout_ms = 300)]
pub struct SetConfirmMode {
    #[at_arg(position = 1)]
    pub mode: ConfirmMode,
}

/// Get Send Confirm Status
#[derive(Clone, AtatCmd)]
#[at_cmd("+CFS=?", ConfirmStatus, timeout_ms = 300)]
pub struct GetConfirmStatus;

/// Request Network Time
#[derive(Clone, AtatCmd)]
#[at_cmd("+TIMEREQ", NoResponse, timeout_ms = 300)]
pub struct RequestNetworkTime {
    #[at_arg(position = 1)]
    pub status: TimeRequestStatus,
}

/// Get Local Time
#[derive(Clone, AtatCmd)]
#[at_cmd("+LTIME=?", NetworkTime, timeout_ms = 300)]
pub struct GetNetworkTime;

/// Get RSSI of the last received packet
#[derive(Clone, AtatCmd)]
#[at_cmd("+RSSI=?", SignalStrength, timeout_ms = 300)]
pub struct GetSignalStrength;

/// Send a short message
#[derive(Clone, AtatCmd)]
#[at_cmd("+SEND", NoResponse, timeout_ms = 300)]
pub struct ShortSend {
    // Application port to be transmitted to
    // #[at_arg(position = 1)]
    // pub port: Bytes<3>,
    // Data in hexadecimal formatted string
    // #[at_arg(position = 2)]
    // pub payload: Bytes<500>,
    #[at_arg(position = 1)]
    pub payload: Bytes<500>,
}
