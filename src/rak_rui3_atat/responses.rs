use atat::atat_derive::AtatResp;
use atat::heapless::String;
use atat::heapless_bytes::Bytes;

#[derive(Clone, AtatResp)]
pub struct NoResponse;

#[derive(Clone, AtatResp)]
pub struct OkResponse {
    pub code: String<2>,
}

#[derive(Clone, Debug, AtatResp)]
pub struct SN {
    pub sn: Bytes<18>,
}

#[derive(Clone, Debug, AtatResp)]
pub struct Version {
    pub ver: Bytes<20>,
}

#[derive(Clone, Debug, AtatResp)]
pub struct DevEUI {
    pub dev_eui: Bytes<16>,
}

#[derive(Clone, Debug, AtatResp)]
pub struct AppEUI {
    pub app_eui: Bytes<16>,
}

#[derive(Clone, Debug, AtatResp)]
pub struct AppKey {
    pub app_key: Bytes<32>,
}

#[derive(Clone, Debug, AtatResp)]
pub struct JoinStatus {
    #[at_arg(position = 1)]
    pub status: u8,
}

#[derive(Clone, Debug, AtatResp)]
pub struct ConfirmStatus {
    #[at_arg(position = 1)]
    pub status: u8,
}

#[derive(Clone, Debug, AtatResp)]
pub struct NetworkTime {
    #[at_arg(position = 1)]
    pub time: Bytes<64>,
}

#[derive(Clone, Debug, AtatResp)]
pub struct SignalStrength {
    #[at_arg(position = 1)]
    pub rssi: Bytes<4>,
}
