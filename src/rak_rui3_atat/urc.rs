use atat::digest::ParseError;
use atat::nom::character;
use atat::{
    nom::{bytes, combinator, sequence},
    AtatUrc, Parser,
};

use crate::digester;

#[derive(Debug, PartialEq, Clone)]
pub enum UrcMessages {
    Unknown,
    Event(EventUrc),
}

impl UrcMessages {}

impl AtatUrc for UrcMessages {
    type Response = Self;

    fn parse(resp: &[u8]) -> Option<Self::Response> {
        match resp {
            b if b.starts_with(b"+EVT:") => EventUrc::parse(resp).ok().map(UrcMessages::Event),
            _ => None,
        }
    }
}

impl Parser for UrcMessages {
    fn parse(buf: &[u8]) -> Result<(&[u8], usize), ParseError> {
        // Is it an URC?
        let (_, (head, data, (t1, t2))) = sequence::tuple((
            combinator::success(&b""[..]),
            combinator::recognize(sequence::tuple((
                bytes::streaming::tag("+"),
                character::streaming::alpha1,
                bytes::streaming::tag(":"),
                bytes::streaming::take_until("\r\n"),
            ))),
            digester::RakRui3Digester::take_until_including("\r\n"),
        ))(buf)?;

        return Ok((data, head.len() + data.len() + t1.len() + t2.len()));
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum EventUrc {
    Joined,
    JoinedFailed,
    TxDone,
    SendOk,
    SendFailed,
    TimeReqOk,
    TimeReqFailed,
}

impl EventUrc {
    pub fn parse(buf: &[u8]) -> Result<Self, ParseError> {
        let (val, _) = sequence::tuple((bytes::streaming::tag("+EVT:"),))(buf)?;

        let ret = match core::str::from_utf8(val) {
            Ok(val) => match val {
                x if x.starts_with("JOINED") => Ok(EventUrc::Joined),
                x if x.starts_with("JOIN_FAILED") => Ok(EventUrc::JoinedFailed),
                x if x.starts_with("TX_DONE") => Ok(EventUrc::TxDone),
                x if x.starts_with("SEND_CONFIRMED_OK") => Ok(EventUrc::SendOk),
                x if x.starts_with("SEND_CONFIRMED_FAILED") => Ok(EventUrc::SendFailed),
                x if x.starts_with("TIMEREQ_OK") => Ok(EventUrc::TimeReqOk),
                x if x.starts_with("TIMEREQ_FAILED") => Ok(EventUrc::TimeReqFailed),
                _ => Err(ParseError::NoMatch),
            },
            _ => Err(ParseError::NoMatch),
        };

        ret
    }
}
