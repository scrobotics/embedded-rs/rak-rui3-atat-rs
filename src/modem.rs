use log::*;

use crate::rak_rui3_atat::types::*;
use crate::rak_rui3_atat::urc::{EventUrc, UrcMessages};
use crate::rak_rui3_atat::*;

use atat::blocking::{AtatClient, Client};
use atat::heapless_bytes::Bytes;
use atat::UrcChannel;
use time;

use crate::ModemError;

pub const INGRESS_BUF_SIZE: usize = 1024;
pub const URC_CAPACITY: usize = 128;
pub const URC_SUBSCRIBERS: usize = 3;

pub struct RakRui3<W: embedded_io::Write> {
    client: Client<'static, W, INGRESS_BUF_SIZE>,
    urc_channel: &'static UrcChannel<UrcMessages, URC_CAPACITY, URC_SUBSCRIBERS>,
    sn: [u8; 18],
}

impl<W: embedded_io::Write> RakRui3<W> {
    pub fn new(
        client: Client<'static, W, INGRESS_BUF_SIZE>,
        urc_channel: &'static UrcChannel<UrcMessages, URC_CAPACITY, URC_SUBSCRIBERS>,
    ) -> Result<Self, ModemError> {
        info!("Initializing RAK RUI3 Modem...");

        let mut driver = RakRui3 {
            client,
            urc_channel,
            sn: [0; 18],
        };

        driver.is_alive()?;
        driver.update_sn()?;
        driver.setup_modem(Some(DataRate::SF7BW125))?;

        Ok(driver)
    }

    fn update_sn(&mut self) -> Result<(), ModemError> {
        match self.client.send(&GetVersion) {
            Ok(res) => {
                log::info!("Version: {:?}", res.ver);
            }
            Err(e) => {
                log::error!("Version not found: {:?}", e);
            }
        }

        let sn = match self.client.send(&GetSN) {
            Ok(sn) => sn.sn,
            Err(e) => {
                log::error!("SN not found: {:?}", e);
                return Err(ModemError::NotResponding);
            }
        };

        // SN may be shorter than 18 bytes (typically 16 bytes)
        let len = sn.len();
        self.sn[..len].copy_from_slice(sn.as_slice());

        let sn_str = std::str::from_utf8(&self.sn).unwrap();
        info!("SN: {}", sn_str);

        Ok(())
    }

    fn is_alive(&mut self) -> Result<(), ModemError> {
        match self.client.send(&AT) {
            Ok(_) => {
                log::info!("Modem alive");
                Ok(())
            }
            Err(e) => {
                log::error!("Modem not alive: {:?}", e);
                Err(ModemError::NotResponding)
            }
        }
    }

    fn setup_modem(&mut self, dr: Option<DataRate>) -> Result<(), ModemError> {
        match self.client.send(&SetConfirmMode {
            mode: ConfirmMode::AckOn,
        }) {
            Ok(_) => {
                log::info!("Confirm mode set");
            }
            Err(e) => {
                log::error!("Configuration failed: {:?}", e);
                return Err(ModemError::NotResponding);
            }
        };

        match self.client.send(&SetDataRate {
            rate: dr.unwrap_or(DataRate::SF7BW125),
        }) {
            Ok(_) => {
                log::info!("Data rate set");
            }
            Err(e) => {
                log::error!("Configuration failed: {:?}", e);
                return Err(ModemError::NotResponding);
            }
        };

        Ok(())
    }

    pub fn get_signal_strength(&mut self) -> Result<(i16, u8), ModemError> {
        match self.client.send(&GetSignalStrength) {
            Ok(res) => {
                let rssi = std::str::from_utf8(res.rssi.as_slice()).unwrap_or("0");
                let rssi = rssi.parse::<i16>().unwrap_or(0);

                let signal = if rssi > -30 {
                    -30
                } else if rssi < -140 {
                    -140
                } else {
                    rssi
                };
                let signal = -100 * (signal + 140) / (-140 + 30);
                log::info!("RSSI: {}dB ({}%)", rssi, signal);
                Ok((rssi, signal as u8))
            }
            Err(e) => {
                log::error!("Failed to get RSSI: {:?}", e);
                log::debug!("Has a packet been already received?");
                Err(ModemError::NotResponding)
            }
        }
    }

    pub fn get_network_time(&mut self) -> Result<i64, ModemError> {
        log::info!("Getting network time...");

        match self.client.send(&RequestNetworkTime {
            status: TimeRequestStatus::Enabled,
        }) {
            Ok(_) => {}
            Err(e) => {
                log::error!("Failed to enable time request: {:?}", e);
                return Err(ModemError::NotResponding);
            }
        };

        // To retrieve the network time, we need to send a dummy message first
        match self.send(
            "_TIMEREQ_".as_bytes(),
            1,
            std::time::Duration::from_secs(20),
        ) {
            Ok(_) => {}
            Err(ModemError::OperationFailed) => {
                log::error!("Failed to send dummy message");
                return Err(ModemError::OperationFailed);
            }
            Err(e) => {
                log::error!("Failed to send dummy message: {:?}", e);
                return Err(ModemError::NotResponding);
            }
        }

        let mut subscriber = self.urc_channel.subscribe().unwrap();
        let now = std::time::SystemTime::now();
        while now.elapsed().unwrap() < std::time::Duration::from_secs(10) {
            std::thread::sleep(std::time::Duration::from_millis(500));

            match subscriber.try_next_message_pure() {
                Some(UrcMessages::Event(EventUrc::TimeReqOk)) => {
                    break;
                }
                Some(UrcMessages::Event(EventUrc::TimeReqFailed)) => {
                    log::error!("Failed to get network time");
                    return Err(ModemError::NotResponding);
                }
                Some(e) => {
                    log::error!("Unknown URC {:?}", e);
                }
                None => {
                    log::debug!("Waiting for response...");
                }
            }
        }

        match self.client.send(&GetNetworkTime) {
            Ok(time) => {
                // Turn "04h36m00s on 11/27/2023" into "11/27/2023T04:36:00Z"
                log::info!("Network time: {:?}", time.time);
                let dt_str =
                    std::str::from_utf8(time.time.as_slice()).unwrap_or("00h00m00s on 01/01/1970");

                let fd = time::format_description::parse(
                    "[hour]h[minute]m[second]s on [month]/[day]/[year]",
                )
                .unwrap();
                let dt = time::PrimitiveDateTime::parse(dt_str, &fd).unwrap();
                let dt = dt.assume_offset(time::UtcOffset::UTC);

                let ts = dt.unix_timestamp();
                log::info!("Timestamp: {}", ts);

                return Ok(ts);
            }
            Err(e) => {
                log::error!("Failed to get time: {:?}", e);
                return Err(ModemError::NotResponding);
            }
        };
    }

    pub fn set_otaa_params(
        &mut self,
        dev_eui: Option<&str>,
        app_eui: Option<&str>,
        app_key: Option<&str>,
    ) -> Result<(), ModemError> {
        if let Some(dev_eui) = dev_eui {
            let dev_eui = Bytes::from_slice(dev_eui.as_bytes()).unwrap();

            match self.client.send(&SetDevEUI { dev_eui }) {
                Ok(_) => {
                    log::info!("DevEUI set");
                }
                Err(e) => {
                    log::error!("Failed to set DevEUI: {:?}", e);
                    return Err(ModemError::NotResponding);
                }
            };
        }

        if let Some(app_eui) = app_eui {
            let app_eui = Bytes::from_slice(app_eui.as_bytes()).unwrap();

            match self.client.send(&SetAppEUI { app_eui }) {
                Ok(_) => {
                    log::info!("AppEUI set");
                }
                Err(e) => {
                    log::error!("Failed to set AppEUI: {:?}", e);
                    return Err(ModemError::NotResponding);
                }
            };
        }

        if let Some(app_key) = app_key {
            let app_key = Bytes::from_slice(app_key.as_bytes()).unwrap();

            match self.client.send(&SetAppKey { app_key }) {
                Ok(_) => {
                    log::info!("AppKey set");
                }
                Err(e) => {
                    log::error!("Failed to set AppKey: {:?}", e);
                    return Err(ModemError::NotResponding);
                }
            };
        }

        Ok(())
    }

    pub fn get_otaa_params(&mut self) -> Result<(), ModemError> {
        let eui = match self.client.send(&GetDevEUI) {
            Ok(pl) => pl.dev_eui,
            Err(e) => {
                log::error!("DevEUI not found: {:?}", e);
                return Err(ModemError::NotResponding);
            }
        };

        let eui_str = std::str::from_utf8(&eui).unwrap();
        info!("DevEUI: {}", eui_str);

        let eui = match self.client.send(&GetAppEUI) {
            Ok(pl) => pl.app_eui,
            Err(e) => {
                log::error!("AppEUI not found: {:?}", e);
                return Err(ModemError::NotResponding);
            }
        };

        let eui_str = std::str::from_utf8(&eui).unwrap();
        info!("AppEUI: {}", eui_str);

        let eui = match self.client.send(&GetAppKey) {
            Ok(pl) => pl.app_key,
            Err(e) => {
                log::error!("AppKey not found: {:?}", e);
                return Err(ModemError::NotResponding);
            }
        };

        let eui_str = std::str::from_utf8(&eui).unwrap();
        info!("AppKey: {}", eui_str);

        Ok(())
    }

    pub fn join_network(&mut self, timeout: std::time::Duration) -> Result<(), ModemError> {
        info!("Joining network...");

        match self.client.send(&JoinNetwork {
            request: JoinRequest::Start,
        }) {
            Ok(_) => {}
            Err(e) => {
                log::error!("Failed to join network: {:?}", e);
                return Err(ModemError::NotResponding);
            }
        };

        let mut subscriber = self.urc_channel.subscribe().unwrap();
        let now = std::time::SystemTime::now();
        while now.elapsed().unwrap() < timeout {
            std::thread::sleep(std::time::Duration::from_millis(500));

            match subscriber.try_next_message_pure() {
                Some(UrcMessages::Event(EventUrc::Joined)) => {
                    break;
                }
                Some(UrcMessages::Event(EventUrc::JoinedFailed)) => {
                    break;
                }
                Some(e) => {
                    log::error!("Unknown URC {:?}", e);
                }
                None => {
                    log::debug!("Waiting for response...");
                }
            }
        }

        match self.client.send(&GetJoinStatus {}) {
            Ok(status) => {
                log::info!("Join status: {:?}", status);
                match status.status {
                    0 => {
                        log::error!("Failed to join network");
                        return Err(ModemError::NoNetwork);
                    }
                    1 => {
                        log::info!("Joined to network");
                    }
                    _ => {
                        unreachable!("Unknown response");
                    }
                }
            }
            Err(e) => {
                log::error!("Join status not found: {:?}", e);
                return Err(ModemError::NotResponding);
            }
        };

        Ok(())
    }

    pub fn send(
        &mut self,
        payload: &[u8],
        port: u8,
        timeout: std::time::Duration,
    ) -> Result<(), ModemError> {
        log::info!("Sending payload...");
        if port == 0 || port > 233 {
            log::error!("Port not available");
            return Err(ModemError::OperationFailed);
        }

        let payload: String = payload
            .iter()
            .map(|c| format!("{:02x}", *c as u8))
            .collect();
        if payload.len() < 2 || payload.len() > 500 {
            // Max payload size actually depend on the data rate, if not valid the command
            // will return AT_PARAM_ERROR URC
            log::error!("Payload size not valid ({})", payload.len());
            return Err(ModemError::OperationFailed);
        }

        let payload = format!("{}:{}", port, payload);
        let payload = Bytes::from_slice(payload.as_bytes()).unwrap();

        match self.client.send(&ShortSend { payload }) {
            Ok(_) => {}
            Err(e) => {
                log::error!("Failed to send payload: {:?}", e);
                return Err(ModemError::NotResponding);
            }
        };

        let mut subscriber = self.urc_channel.subscribe().unwrap();
        let now = std::time::SystemTime::now();
        while now.elapsed().unwrap() < timeout {
            std::thread::sleep(std::time::Duration::from_millis(500));

            match subscriber.try_next_message_pure() {
                Some(UrcMessages::Event(EventUrc::TxDone)) => {
                    break;
                }
                Some(UrcMessages::Event(EventUrc::SendOk)) => {
                    break;
                }
                Some(UrcMessages::Event(EventUrc::SendFailed)) => {
                    info!("Message not sent");
                    return Err(ModemError::OperationFailed);
                }
                Some(e) => {
                    log::error!("Unknown URC {:?}", e);
                }
                None => {
                    log::debug!("Waiting for response...");
                }
            }
        }

        match self.client.send(&GetConfirmStatus {}) {
            Ok(status) => match status.status {
                0 => {
                    log::error!("Message not sent");
                    return Err(ModemError::OperationFailed);
                }
                1 => {
                    log::info!("Message sent");
                }
                _ => {
                    unreachable!("Unknown response");
                }
            },
            Err(e) => {
                log::error!("Confirm status not found: {:?}", e);
                return Err(ModemError::NotResponding);
            }
        };

        Ok(())
    }
}

#[cfg(test)]
mod tests {

    #[test]
    fn test_get_network_time() {
        let fd =
            time::format_description::parse("[hour]h[minute]m[second]s on [month]/[day]/[year]")
                .unwrap();
        let t = time::PrimitiveDateTime::parse("04h36m00s on 11/27/2023", &fd).unwrap();
        let t = t.assume_offset(time::UtcOffset::UTC);

        let ts = t.unix_timestamp();
        log::info!("Timestamp: {}", ts);

        assert_eq!(ts, 1701059760);
    }
}
