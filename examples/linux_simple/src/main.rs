use rak_rui3_eh_driver::digester::RakRui3Digester;
use rak_rui3_eh_driver::modem::RakRui3;
use rak_rui3_eh_driver::modem::INGRESS_BUF_SIZE;
use rak_rui3_eh_driver::modem::URC_CAPACITY;
use rak_rui3_eh_driver::modem::URC_SUBSCRIBERS;
use rak_rui3_eh_driver::rak_rui3_atat::urc::UrcMessages;

use atat::blocking::Client;
use atat::AtatIngress;
use atat::Ingress;
use atat::{Config as AtatConfig, ResponseSlot, UrcChannel};

use embedded_io::Read;
use serialport;
use static_cell::StaticCell;

#[toml_cfg::toml_config]
struct Config {
    #[default("")]
    dev_eui: &'static str,
    #[default("")]
    app_eui: &'static str,
    #[default("")]
    app_key: &'static str,
}

fn main() {
    env_logger::builder()
        .filter_level(log::LevelFilter::Debug)
        .init();

    // Open serial port
    let serial_tx = serialport::new("/dev/ttyUSB0", 115_200)
        .timeout(std::time::Duration::from_millis(1000))
        .open()
        .expect("Could not open serial port");
    let serial_rx = serial_tx.try_clone().expect("Could not clone serial port");

    let serial_tx = embedded_io_adapters::std::FromStd::new(serial_tx);
    let mut serial_rx = embedded_io_adapters::std::FromStd::new(serial_rx);

    static INGRESS_BUF: StaticCell<[u8; INGRESS_BUF_SIZE]> = StaticCell::new();
    static RES_SLOT: ResponseSlot<INGRESS_BUF_SIZE> = ResponseSlot::new();
    static URC_CHANNEL: UrcChannel<UrcMessages, URC_CAPACITY, URC_SUBSCRIBERS> = UrcChannel::new();
    let mut ingress = Ingress::new(
        RakRui3Digester::default(),
        INGRESS_BUF.init([0; INGRESS_BUF_SIZE]),
        &RES_SLOT,
        &URC_CHANNEL,
    );

    static BUF: StaticCell<[u8; 1024]> = StaticCell::new();
    let buf = BUF.init([0; 1024]);

    let client = Client::new(serial_tx, &RES_SLOT, buf, AtatConfig::default());

    log::info!("Starting ATAT loop...");
    let _ = std::thread::spawn(move || loop {
        let buf = ingress.write_buf();
        match serial_rx.read(buf) {
            Ok(len) => {
                if len != 0 {
                    // let s = std::str::from_utf8(&buf[..len]).unwrap();
                    // log::debug!("Read: ({}) {}", len, s);
                }
                match ingress.try_advance(len) {
                    Ok(_) => {}
                    Err(e) => {
                        log::info!("Error advancing ingress {:?}", e);
                        ingress.clear();
                    }
                }
            }
            Err(e) => {
                log::info!("Error reading from UART: {:?}", e);
                ingress.clear();
            }
        }
    });
    // end of atat initialization

    log::info!("Starting ATAT client...");
    let mut mm = match RakRui3::new(client, &URC_CHANNEL) {
        Ok(mm) => {
            log::info!("Modem initialized");
            mm
        }
        Err(e) => {
            log::info!("Error initializing modem: {:?}", e);
            loop {
                std::thread::sleep(std::time::Duration::from_millis(1000));
            }
        }
    };

    log::info!("Setting network parameters...");
    mm.set_otaa_params(
        Some(CONFIG.dev_eui),
        Some(CONFIG.app_eui),
        Some(CONFIG.app_key),
    )
    .unwrap();

    log::info!("Joining network...");
    mm.join_network(std::time::Duration::from_secs(20)).unwrap();

    log::info!("Getting network time...");
    mm.get_network_time().unwrap();

    log::info!("Sending data...");
    mm.send(
        "Ola Mundo".as_bytes(),
        5,
        std::time::Duration::from_secs(20),
    )
    .unwrap();
}
